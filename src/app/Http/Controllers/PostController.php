<?php

namespace App\Http\Controllers;

use App\Http\Requests\Post\Store;
use App\Http\Requests\Post\Update;
use App\Models\Post;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Post::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Store $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Store $request)
    {
        $data = $request->validated();
        $post = Post::create($data);
        $post->save();

        return response()->json($post);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $post = $this->getPostOr404($id);

        return response()->json($post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request, $id)
    {
        $post = $this->getPostOr404($id);
        $post->update($request->validated());
        $post->save();

        return response()->json($post);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = $this->getPostOr404($id);

        $post->delete();

        return response(null, 200);
    }

    /**
     * @param $id
     *
     * @return Post
     */
    private function getPostOr404($id): Post
    {
        $post = Post::find($id);
        if (empty($post)) {
            abort(404);
        }

        return $post;
    }
}
