<?php

namespace App\Http\Requests\Post;

class Update extends Store
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), ['id' => 'require|integer']);
    }
}
