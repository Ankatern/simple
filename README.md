Install:
1) install docker
2) cd src -> composer install
3) cp .evn.test .env
4) docker-compose build && docker-compose up -d
5) docker-compose exec php php /var/www/html/artisan migrate
6) Go to http://localhost:8088

//TODO
1) Add composer to docker
2) Add validation user for api

Methods:

Get all posts - GET /api/post

Get post by id - GET /api/post/{id}

Delete post by id - DELETE /api/post/{id}

Create post - POST /api/post
headers:

Content-Type: application/json

Accept: application/json

Body:
{

	"title": "test",
	"text": "test",
	"description": "test"
} 

Create post - PUT /api/post/{id}
headers:
Content-Type: application/json
Accept: application/json


{
	
	"title": "test",
	"text": "test131311",
	"description": ""
}
